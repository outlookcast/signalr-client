import React, { Component } from 'react';
import { Button, Typography, LinearProgress, Card, CardContent } from '@material-ui/core';
import axios from 'axios';
const moment = require('moment');
const signalR = require("@aspnet/signalr");

class Home extends Component {
    constructor(props) {
        super(props);

        this.state = {
            files: [],
            connection: null,
            importacao: []
        }
    }

    handleChande = (e) => {
        this.setState({
            files: e.target.files
        }, () => console.log(this.state.files));
    }

    // Iniciamos a conexão com o hub
    async componentDidMount() {
        this.setState({
            connection: new signalR.HubConnectionBuilder()
                .withUrl("http://localhost:5000/hubs/importacao", {
                    transport: signalR.HttpTransportType.WebSockets | signalR.HttpTransportType.LongPolling,
                    'content-type': 'application/json',
                    accessTokenFactory: () => localStorage.getItem('token')
                })
                .configureLogging(signalR.LogLevel.Information)
                .build()
        }, async () => {
            await this.state.connection.start();

            this.state.connection.on("MensagensImportacao", data => {
                const obj = JSON.parse(data);
                this.handleNewMessage(obj);
            });
        });
    }

    // Função para tratar as mensagens que chegam do backend
    handleNewMessage(obj) {
        console.log('Mensagem recebida: ', obj)
        const existeImportacao = !!this.state.importacao.find(x => x.IdImportacao === obj.IdImportacao);

        if (!existeImportacao) {
            const arr = this.state.importacao;

            arr.push(obj);

            this.setState({
                importacao: arr
            });
        } else {
            let arrUpdate = this.state.importacao;
            arrUpdate = arrUpdate.filter(item => item.IdImportacao !== obj.IdImportacao);
            arrUpdate.push(obj);
            this.setState({
                importacao: arrUpdate
            });
        }

        if (obj.PorcentagemConclusao >= 100) {
            // Espera 3 segundos para remover a importação
            setTimeout(() => {
                this.removerArray(obj);
            }, 1000 * 3)
        }
    }

    // Fechamos a conexão quando o usuário sair da página
    componentWillUnmount() {
        this.state.connection.stop();
        this.setState({
            connection: null
        });
    }

    // Em caso de erro, também devemos fechar a conexão
    componentDidCatch() {
        this.state.connection.stop();
        this.setState({
            connection: null
        });
    }

    removerArray(valor) {
        let arr = this.state.importacao;
        arr = arr.filter(item => item.IdImportacao !== valor.IdImportacao);

        this.setState({
            importacao: arr
        });
    }

    async upload() {
        if(Array.from(this.state.files).length) {
            const formData = new FormData();
            Array.from(this.state.files).forEach(file => {
                formData.append('file', file);
            });
            await axios.post('http://localhost:5000/api/importacao', formData, {
                headers: {
                    'content-type': 'multipart/form-data',
                    'Authorization': 'Bearer ' + localStorage.getItem('token')
                },
                withCredentials: true
            }).catch(err => {
                console.log('Erro ao chamar API => ', err)
            });
        } else{
            alert('Você precisa selecionar pelo menos um arquivo!')
        }
    }


    render() {
        return (
            <div style={{
                textAlign: 'center'
            }}>
                <Card style={{
                    margin: '50px'
                }}>
                    <CardContent>
                        <form>
                            <h1 component="h1">
                                Upload de arquivos
                            </h1>
                            <input
                                type='file'
                                multiple
                                onChange={this.handleChande}
                            />
                            <div>
                                <Button variant="contained" color="secondary" style={{ marginTop: '20px' }} onClick={() => this.upload()}>Enviar</Button>
                            </div>
                        </form>
                    </CardContent>
                </Card>

                <Card style={{ margin: '50px' }}>
                    <CardContent>
                        <h3>Importações em andamento</h3>
                        {this.state.importacao.length ? (this.state.importacao.sort((a, b) => (a.IdImportacao > b.IdImportacao) - (a.IdImportacao < b.IdImportacao)).map(obj => {
                            return (
                                <Card key={obj.IdImportacao} style={{
                                    margin: '50px'
                                }}>
                                    <CardContent>
                                        <Typography variant="h5" component="h2">
                                            Importação: {obj.IdImportacao}
                                        </Typography>
                                        <p><b>Última atualização:</b> {moment(obj.Horario, 'YYYY-MM-DDTHH:mm:ss').format('DD/MM/YYYY - HH:mm:ss')}</p>
                                        <p><b>Mensagem:</b> {obj.MensagemLog}</p>
                                        <p><b>Progresso:</b> {obj.PorcentagemConclusao}%</p>

                                        <LinearProgress variant="determinate" value={obj.PorcentagemConclusao} />
                                    </CardContent>
                                </Card>
                            );
                        })) : <p>Não existem importações em andamento</p>}
                    </CardContent>
                </Card>

            </div>
        );
    }
}

export default Home;