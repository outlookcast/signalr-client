import React, { Fragment } from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import Login from './Componentes/Login';
import { isAuthenticated } from './auth';
import Home from './Componentes/Home';
import Administration from './Componentes/Administration';
import Header from "./Componentes/Header";


const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        isAuthenticated() ? (
            <Component {...props} />
        ) : (
                <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
            )
    )} />
);

const Routes = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path="/login" component={Login} />
            <Fragment>
                <Header />
                <PrivateRoute exact path="/" component={Home} />
                <PrivateRoute exact path="/home" component={Home} />
                <PrivateRoute exact path="/administration" component={Administration} />
            </Fragment>
        </Switch>
    </BrowserRouter>
)

export default Routes;